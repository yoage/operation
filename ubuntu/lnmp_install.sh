#!/bin/bash
#Script:lnmp_install command for ubuntu apt-get
#Version:1.0
#@author Andy Wong <yooage@gmail.com>


sudo apt-get update  
sudo apt-get install php5-common php5-mysqlnd php5-xmlrpc php5-curl php5-gd php5-cli php5-fpm php-pear php5-dev php5-imap php5-mcrypt -y
sudo apt-get install nginx -y
sudo apt-get install mysql-server -y
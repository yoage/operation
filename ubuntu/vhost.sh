#!/bin/bash
#Script:vhost command for ubuntu apt-get
#Version:1.0
#@author Andy Wong <yooage@gmail.com>


# Check if user is root
if [ $(id -u) != "0" ]; then
    echo "Error: You must be root to run this script, use sudo sh $0"
    exit 1
fi

if [ "$1" != "--help" ]; then
	domain="www.myweb.com"
	echo "Please input domain:"
	read -p "(Default domain: www.myweb.com):" domain
	if [ "$domain" = "" ]; then
		domain="www.myweb.com"
	fi
	if [ ! -f "/etc/nginx/sites-available/$domain.conf" ]; then
	echo "==========================="
	echo "domain=$domain"
	echo "===========================" 
	else
	echo "==========================="
	echo "$domain is exist!"
	echo "==========================="	
	fi
	
	echo "Do you want to add more domain name? (y/n)"
	read add_more_domainame

	if [ "$add_more_domainame" == 'y' ]; then

	  echo "Type domainname,example(bbs.myweb.com forums.myweb.com luntan.myweb.com):"
	  read moredomain
          echo "==========================="
          echo domain list="$moredomain"
          echo "==========================="
	  moredomainame=" $moredomain"
	fi

	vhostdir="/var/www/$domain"
	echo "Please input the directory for the domain:$domain :"
	read -p "(Default directory: /var/www/$domain):" vhostdir
	if [ "$vhostdir" = "" ]; then
		vhostdir="/var/www/$domain"
	fi
	echo "==========================="
	echo Virtual Host Directory="$vhostdir"
	echo "==========================="


	echo "==========================="
	echo "Allow access_log? (y/n)"
	echo "==========================="
	read access_log

	if [ "$access_log" == 'n' ]; then
	  access_log_conf="access_log off;"
	else

		vhostLogDir="/var/www/wwwlogs"
		echo "Please input the directory for the logs :$domain.log :"
		read -p "(Default directory: /var/www/wwwlogs):" vhostLogDir
		if [ "$vhostLogDir" = "" ]; then
			vhostLogDir="/var/www/wwwlogs"
		fi


	  echo "Type access_log name(Default access log file:$domain.log):"
	  read al_name
	  if [ "$al_name" = "" ]; then
		al_name="$domain"
	  fi
	  access_log_conf="access_log  $vhostLogDir"/"$domain.log  access;"
	echo "==========================="
	echo You access log file="$al_name.log"
	echo "==========================="
	fi

	get_char()
	{
		SAVEDSTTY=`stty -g`
		stty -echo
		stty cbreak
		dd if=/dev/tty bs=1 count=1 2> /dev/null
		stty -raw
		stty echo
		stty $SAVEDSTTY
	}
	echo ""
	echo "Press any key to start create virtul host..."
	char=`get_char`


if [ ! -d $vhostLogDir ]; then
	mkdir -p $vhostLogDir

	chmod 777 $vhostLogDir
fi

echo "Create Virtul Host directory......"
echo $vhostdir 

mkdir -p $vhostdir

touch $vhostLogDir/$domain.log 
chown -R www-data:www-data $vhostLogDir/$domain.log 
echo "set permissions of Virtual Host directory......"
chmod -R 755 $vhostdir
chown -R www-data:www-data $vhostdir

cat >"/etc/nginx/sites-available/"$domain.conf<<eof
server {
	listen 80;
	#listen [::]:80;
	
	root  $vhostdir;
	index index.html index.php;

	# Make site accessible from $domain$moredomainame;
	server_name $domain$moredomainame;

	#error_page   404   /404.html;

	location ~ [^/]\.php(/|$) {
		# comment try_files \$uri =404; to enable pathinfo
		try_files \$uri =404;

		fastcgi_pass unix:/var/run/php5-fpm.sock;

		fastcgi_index index.php;

		include fastcgi_params;
	}

	location ~ .*\.(gif|jpg|jpeg|png|bmp|swf)$ {
		expires	30d;
	}

	location ~ .*\.(js|css)?$ {
		expires	12h;
	}

	$access_log_conf
}
eof


cur_php_version=`php  -r 'echo PHP_VERSION;'`

if echo "$cur_php_version" | grep -qE "5.3.|5.4.|5.5."
then
cat >>/etc/php5/fpm/php.ini<<eof
[HOST=$domain]
open_basedir=$vhostdir/:/tmp/
[PATH=$vhostdir]
open_basedir=$vhostdir/:/tmp/
eof
service php5-fpm restart
fi

ln -s  "/etc/nginx/sites-available/"$domain.conf "/etc/nginx/sites-enabled/"$domain.conf

echo "Test Nginx configure file......"
/usr/sbin/nginx  -t
echo ""
echo "Restart Nginx......"
 service nginx reload
fi